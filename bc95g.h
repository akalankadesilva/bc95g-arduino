#include <Stream.h>
#include <Arduino.h>
#define ENABLE_NB_TIMEOUT 25000
#define REG_NB_TIMEOUT 300000

#define RESPONSE_PENDING -1
#define RESPONSE_COMPLETE 1
#define RESPONSE_TIMEOUT 2
#define RESPONSE_ERROR 3

#define DEBUG_OFF 0
#define DEBUG_STATUS 1
#define DEBUG_AT 2
#define DEBUG_RAW 3

#define CMD_BUFF_LEN 128

class bc95g{
  public:
    
    void init(Stream &serial);
    void setDebug(Stream &serial,int level);
    unsigned long getTime();
    int readLine(char* line);
    void shiftBuffer(char* input,int start,int shift,int length);
	void clearBuffer(char* input,int start,int end);
    int waitResponse(char* response,unsigned long timeout,int length=-1);
    void checkUrc(char* modemResponse);
    bool reboot();
    bool sleep();
    bool wake();
    bool isRegistered();
    bool registerNB(int mcc=-1,int mnc=-1);
    bool setFunction(unsigned char mode);
    bool getIMEI(char* imei);
    bool getIMSI(char* imsi);
    bool setAPN(char* apn);
    bool enableAutoConnect();
    bool disableAutoConnect();
    bool checkNB();
    bool connectMqtt(char* host,int port,int connectionId,char* username,char* password);
    bool disconnectMqtt(int connectionId);
    bool publishMqtt(int connectionId,int msgId,int qos,int retain,char* topic,char* msg,int msgLength);
	bool psmEnable();
  private:
    Stream* MODEM_SERIAL;
    Stream* DEBUG_SERIAL;
    char cmdBuffer[CMD_BUFF_LEN];
    int cmdBufferLength=0;
    char lastResponse[128];
    bool recvFlag=false;
    int debug=0;
};
