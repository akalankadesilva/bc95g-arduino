#include "bc95g.h"

void bc95g::setDebug(Stream &serial,int level){
	DEBUG_SERIAL=&serial;
	debug=level;
}
unsigned long bc95g::getTime(){
	return millis();
}
int bc95g::readLine(char* line){
	int readLength=0;
	if((readLength=MODEM_SERIAL->available())>0){

		if(readLength+cmdBufferLength>CMD_BUFF_LEN){
			int overflow=readLength+cmdBufferLength-CMD_BUFF_LEN;
			shiftBuffer(cmdBuffer,overflow,-overflow,CMD_BUFF_LEN-overflow);
			cmdBufferLength=-overflow;
		}
		char* writePtr=cmdBuffer+cmdBufferLength;
		MODEM_SERIAL->readBytes(writePtr,readLength);
		cmdBufferLength+=readLength;
		cmdBuffer[cmdBufferLength]=0;
		if(debug>=DEBUG_RAW)
			for(int i=0;i<cmdBufferLength;i++){
				DEBUG_SERIAL->print(cmdBuffer[i],HEX);
				DEBUG_SERIAL->print(" ");
			}
	
	}
	/*
	if(cmdBufferLength>0){
 		char* ptr;
		if((ptr=strchr(cmdBuffer,'\r'))==NULL){
			return 0;
		}
		else{
			int leLength=1;
			if(*(ptr+1)=='\n')
				leLength=2;
			int cmdLength=ptr-cmdBuffer;
			strncpy(line,cmdBuffer,cmdLength);
			line[cmdLength]=0;
			if(debug>=DEBUG_AT)
				DEBUG_SERIAL->println(line);
			shiftBuffer(cmdBuffer,cmdLength+leLength,-cmdLength-leLength,CMD_BUFF_LEN-cmdLength-leLength);
			cmdBufferLength=cmdBufferLength-cmdLength-leLength;
			clearBuffer(cmdBuffer,cmdBufferLength,CMD_BUFF_LEN);
			return cmdLength;
		}
		
	}*/
	  if(cmdBufferLength>0){
    char* ptr;
    for(int i=0;i<cmdBufferLength;i++){
      if(cmdBuffer[i]=='\r'){
        ptr=cmdBuffer+i;
        break;
      }
      if(i==cmdBufferLength-1)
        return 0;
    }
    
    
    int leLength=1;
    if(*(ptr+1)=='\n') 
      leLength=2;
    int cmdLength=ptr-cmdBuffer;
    if(cmdBuffer[0]=='\n'){
      strncpy(line,cmdBuffer+1,cmdLength-1);
      cmdLength-=1;
    }
    else
      strncpy(line,cmdBuffer,cmdLength);

    line[cmdLength]=0;
    if(debug>=DEBUG_AT)
      DEBUG_SERIAL->println(line);
    
    shiftBuffer(cmdBuffer,cmdLength+leLength,-cmdLength-leLength,CMD_BUFF_LEN-cmdLength-leLength);
    cmdBufferLength=cmdBufferLength-cmdLength-leLength;
    clearBuffer(cmdBuffer,cmdBufferLength,CMD_BUFF_LEN);
    return cmdLength;
    
    
  }
	return 0;
}
void bc95g::shiftBuffer(char* input,int start,int shift,int length){
	for(int i=0;i<length;i++){
		input[start+shift+i]=input[start+i];
	}
}
void bc95g::clearBuffer(char* input,int start,int end){
	for(int i=start;i<end;i++){
		input[i]=0;
	}
}


int bc95g::waitResponse(char* response,unsigned long timeout,int length){
  if(length==-1)
    length=strlen(response);
	
	unsigned long startTime=getTime();
	int readLength=readLine(lastResponse);
	while(getTime()-startTime<timeout){
		if(readLength>0){
			if(strncmp(lastResponse,response,length)==0){
				return RESPONSE_COMPLETE;
			}
			else if(strcmp(lastResponse,"ERROR")==0){
				return RESPONSE_ERROR;
			}
			else{
				checkUrc(lastResponse);
			}

		}
		readLength=readLine(lastResponse);

	}
	return RESPONSE_TIMEOUT;
}
void bc95g::checkUrc(char* modemResponse){
	
}

void bc95g::init(Stream &serial) {
  MODEM_SERIAL = &serial;
}

bool bc95g::reboot() {
  MODEM_SERIAL->println("AT+NRB");
  if(waitResponse("REBOOTING", 10000)==RESPONSE_COMPLETE)
  		if(waitResponse("OK", 10000)==RESPONSE_COMPLETE){
  			if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: Reboot complete");
  			return true;
  		}
 return false;
 
}

bool bc95g::sleep() {
  MODEM_SERIAL->println("AT+CFUN=0");
  return waitResponse("OK", 2000) == RESPONSE_COMPLETE;
}
bool bc95g::wake() {
  MODEM_SERIAL->println("AT+CFUN=1");
  return waitResponse("OK", 10000) == RESPONSE_COMPLETE;
}
bool bc95g::isRegistered() {
  MODEM_SERIAL->println("AT+CEREG?");
  if(waitResponse("+CEREG:", 2000,7)==RESPONSE_COMPLETE){
  	int i1=0;
  	int i2=0;
  	if(sscanf(lastResponse,"+CEREG:%d,%d",&i1,&i2)==2){
  		Serial.println(i1);
  		Serial.println(i2);
  		if(i2==1){
  			waitResponse("OK",5000);
  			return true;
  		}
  	}
  	else{
  		Serial.println("Arg count mismatch");
  	}
  }
  return false;
}

bool bc95g::psmEnable()
{
MODEM_SERIAL->println(F("AT+NPSMR=1"));
 waitResponse("OK",10000);
MODEM_SERIAL->println(F("AT+CPSMS=1,,,01000001,00000001"));
  return waitResponse("OK", 10000)==RESPONSE_COMPLETE;	
	
}


bool bc95g::registerNB(int mcc,int mnc) {
  char cmd[32];
  if(mcc!=-1&&mnc!=-1)
  	sprintf(cmd,"AT+COPS=1,2,\"%d%02d\",9",mcc,mnc);
  else
  	sprintf(cmd,"AT+COPS=0");
  
   MODEM_SERIAL->println(cmd);
   waitResponse("OK",10000);
   MODEM_SERIAL->println("AT+CEREG=0");
   waitResponse("OK",10000);
   MODEM_SERIAL->println("AT+CGATT=1");
   waitResponse("OK",10000);
   

   long start = millis();
  while (millis() - start < REG_NB_TIMEOUT) {
    if(isRegistered()){
    	if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: NB registered");
    	return true;
    }
    delay(5000);
  }
  if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: NB register failed");
  return false;
}
bool bc95g:: setFunction(unsigned char mode)
{
  MODEM_SERIAL->print(F("AT+CFUN="));
  MODEM_SERIAL->println(mode);
  return waitResponse("OK", 5000)==RESPONSE_COMPLETE;
}
bool bc95g:: getIMEI(char* imei)
{
  
  MODEM_SERIAL->println(F("AT+CGSN=1"));
  if(waitResponse("+CGSN:", 2000, 6)==RESPONSE_COMPLETE){
  	strcpy(imei,lastResponse+6);
  	return true;
  }
  return false;
}

bool bc95g:: getIMSI(char* imsi)
{
  
  MODEM_SERIAL->println(F("AT+CIMI"));
  waitResponse("AT+CIMI",5000);
  bool status=waitResponse("",5000);
  strcpy(imsi,lastResponse);
  return status;
}


bool bc95g:: setAPN(char* apn)
{
  char cmd[64];
  sprintf(cmd,"AT+CGDCONT=1,\"IP\",\"%s\"",apn);
  MODEM_SERIAL->println(cmd);
  return waitResponse("OK", 5000) == RESPONSE_COMPLETE;
}

bool bc95g:: enableAutoConnect()
{
  MODEM_SERIAL->println(F("AT+NCONFIG=AUTOCONNECT,TRUE"));
  return waitResponse("OK", 5000) == RESPONSE_COMPLETE;
}

bool bc95g:: disableAutoConnect()
{
  MODEM_SERIAL->println(F("AT+NCONFIG=AUTOCONNECT,FALSE"));
  return waitResponse("OK", 5000) == RESPONSE_COMPLETE;
}

bool bc95g::checkNB() {
  MODEM_SERIAL->println(F("AT+CGATT?"));
  if (waitResponse("OK", 5000) == RESPONSE_COMPLETE) {
  	int i1=0;
    if(sscanf(lastResponse,"AT+CGATT:%d",&i1)==1)
    	return i1==1;
  }
return false;
}


//=================================MQTT Functions========================================




bool bc95g::connectMqtt(char* host,int port,int connectionId,char* username,char* password){

	char cmd[128];
	sprintf(cmd,"AT+QMTOPEN=%d,\"%s\",%d",connectionId,host,port);
	MODEM_SERIAL->println(cmd);
	if(waitResponse("OK",5000)==RESPONSE_COMPLETE){
		if(waitResponse("+QMTOPEN:",10000,9)==RESPONSE_COMPLETE){
			int i1=-1;
			int i2=-1;
			if(sscanf(lastResponse,"+QMTOPEN:%d,%d",&i1,&i2)==2){
				if(i2==0){
					sprintf(cmd,"AT+QMTCONN=%d,atl_%08lu,\"%s\",\"%s\"",connectionId,millis(),username,password);
					MODEM_SERIAL->println(cmd);
					if(waitResponse("OK",5000)==RESPONSE_COMPLETE){

						if(waitResponse("+QMTCONN:",20000,9)==RESPONSE_COMPLETE){
							int j1=-1;
							int j2=-1;
							int j3=-1;
							if(sscanf(lastResponse,"+QMTCONN:%d,%d,%d",&j1,&j2,&j3)>=2){
								return j3==0;
							}
						}
					}
				}
			}
		}
	}
	Serial.println("MQTTCON Failed...");
	return false;
}
bool bc95g::disconnectMqtt(int connectionId){
	char cmd[64];
	sprintf(cmd,"AT+QMTDISC=%d",connectionId);
	MODEM_SERIAL->println(cmd);
	if(waitResponse("OK",5000)==RESPONSE_COMPLETE){
		if(waitResponse("+QMTDISC:",10000,10)==RESPONSE_COMPLETE){
			int i1=-1;
			int i2=-1;
			if(sscanf(lastResponse,"+QMTDISC:%d,%d",&i1,&i2)==2){
				if(i2==0){
					/*sprintf(cmd,"AT+QMTCLOSE=%d",connectionId);
					MODEM_SERIAL->println(cmd);
					int j1=-1;
					int j2=-1;
					if(waitResponse("OK",5000)==RESPONSE_COMPLETE){
						if(sscanf(lastResponse,"+QMTCLOSE:%d,%d",&j1,&j2)==2){
							return j2==0;
						}
					}*/
					return true;
				}
			}
		}
	}
	return false;
}
bool bc95g::publishMqtt(int connectionId,int msgId,int qos,int retain,char* topic,char* msg,int msgLength){
	char cmd[64];
	sprintf(cmd,"AT+QMTPUB=%d,%d,%d,%d,\"%s\"",connectionId,msgId,qos,retain,topic);
	MODEM_SERIAL->println(cmd);
	unsigned long start=getTime();
	while(getTime()-start<5000){
		if(MODEM_SERIAL->available()){
			if(MODEM_SERIAL->read()=='>')
				break;
		}
	}
	if(getTime()-start>=5000)
		return false;
	
	MODEM_SERIAL->write(msg,msgLength);
	MODEM_SERIAL->write(0x1A);
	if(waitResponse("OK",10000)==RESPONSE_COMPLETE){
		if(waitResponse("+QMTPUB:",10000,8)==RESPONSE_COMPLETE){
			int i1=-1;
			int i2=-1;
			int i3=-1;
			int i4=-1;
			if(sscanf(lastResponse,"+QMTPUB:%d,%d,%d,%d",&i1,&i2,&i3,&i4)>=3){
				return i3==0;
			}
		}
	}
	
	return false;
}
