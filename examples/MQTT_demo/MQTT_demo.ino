#include "bc95g.h"
#include <NewPing.h>

#define BATT_PIN 0
#define TRIGGER_PIN  17 // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     24 // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define MQTT_CONN_RETRY 3
#define MQTT_PUB_RETRY 3
#define PUB_INTERVAL 30000
#define STATE_INIT 0
#define STATE_NB_REG 1
#define STATE_MQTT_CONNECTED 2

#define APN "nbiot"
#define TOPIC_PUB "generic_brand_1117/generic_device/v1/common"
#define MQTT_USERNAME "generic_brand_1117-generic_device-v1_1881"
#define MQTT_PASSWORD "1557728175_1881"
#define MAC "5456653195788108"
unsigned long lastPub = 80000,lastIndicator;
bc95g modem;
int state = STATE_INIT;
char txBuffer[128];
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  // put your setup code here, to run once:
   pinMode(33,OUTPUT);
  digitalWrite(33,LOW);
  delay(1000);
  digitalWrite(33,HIGH);
  delay(1000);
  Serial.begin(9600);
  Serial.println("Starting..");
 
  
  Serial2.begin(9600);
  modem.init(Serial2);
  modem.setDebug(Serial, DEBUG_AT);


}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial2.available()) {
    int inByte = Serial2.read();
    Serial.write(inByte);
  }

  // read from port 0, send to port 1:
  if (Serial.available()) {
    int inByte = Serial.read();
    Serial2.write(inByte);
  }

indicator(200);
  
  if (millis() - lastPub > PUB_INTERVAL || millis() < lastPub) {
    
    switch (state) {
      case STATE_INIT:
        modem.reboot();
        delay(3000);
        modem.setFunction(1);
        modem.setAPN(APN);
        if (modem.registerNB())
          state = STATE_NB_REG;
        break;
      case STATE_NB_REG:
        if (mqttConnect())
          state = STATE_MQTT_CONNECTED;
        else if (!modem.isRegistered())
          state = STATE_INIT;
        break;
      case STATE_MQTT_CONNECTED:
        if (!mqttPub()) {
          modem.disconnectMqtt(0);
          state = STATE_NB_REG;
        }
        break;
    };
    lastPub = millis();
  }
}

int getUSLevel(){
  return (int)(sonar.convert_cm(sonar.ping_median(5)));
}
int getBatteryLevel(){
  return (int)(analogRead(BATT_PIN));
}

bool mqttConnect() {
  bool stat=false;
  Serial.println("Connecting MQTT");
  for (int i = 0; i < MQTT_CONN_RETRY; i++) {
    stat = modem.connectMqtt("52.221.141.22", 1883, 0, MQTT_USERNAME, MQTT_PASSWORD);
    if (stat)
      break;
  }

  return stat;
}
bool mqttPub() {
  bool stat=false;
  Serial.print(millis()/1000);
  Serial.println(" Publishing MQTT");
  sprintf(txBuffer,"{\"eventName\":\"LevelUpdate\",\"status\":\"1\",\"level\":%d,\"batt\":%d,\"mac\":\"%s\"}",getUSLevel(),getBatteryLevel(),MAC);
  for (int i = 0; i < MQTT_PUB_RETRY; i++) {   
    stat = modem.publishMqtt(0, 0, 0, 0, TOPIC_PUB, txBuffer, strlen(txBuffer));
    if (stat)
      break;
  }

  return stat;
}

void indicator(int duration)
{
    
  if (millis() - lastIndicator> duration || millis() < lastIndicator) 
  {
  digitalWrite(33,!digitalRead(33));
  lastIndicator=millis();
  }
    
  
}
